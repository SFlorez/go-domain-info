package app

import (
	"log"

	"database/sql"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"

	"../database"
	"./controllers"
)

// App Struct to hold refs of router and database
type App struct {
	Router *fasthttprouter.Router
	DB     *sql.DB
}

// RequestHandlerFunction type
type RequestHandlerFunction func(ctx *fasthttp.RequestCtx, db *sql.DB)

var (
	corsAllowHeaders     = "Authorization,Accept,Content-Type"
	corsAllowMethods     = "HEAD,GET,POST,PUT,DELETE,OPTIONS"
	corsAllowOrigin      = "*"
	corsAllowCredentials = "true"
)

// CORS function
func CORS(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {

		ctx.Response.Header.Set("Access-Control-Allow-Credentials", corsAllowCredentials)
		ctx.Response.Header.Set("Access-Control-Allow-Headers", corsAllowHeaders)
		ctx.Response.Header.Set("Access-Control-Allow-Methods", corsAllowMethods)
		ctx.Response.Header.Set("Access-Control-Allow-Origin", corsAllowOrigin)

		next(ctx)
	}
}

// Initialize Create database connection, set up routing and logging
func (a *App) Initialize() {

	a.DB = database.GetDB()

	a.Router = fasthttprouter.New()
	a.initializeRoutes()
}

// initializeRoutes function
func (a *App) initializeRoutes() {
	a.Router.GET("/", a.handleRequest(controllers.Index))
	a.Router.GET("/api/health", a.handleRequest(controllers.HealthStatus))
	a.Router.GET("/api/domains", a.handleRequest(controllers.GetAllDomains))
	a.Router.GET("/api/domains/analyze", a.handleRequest(controllers.GetADomain))
}

// Run function
func (a *App) Run(host string) {
	log.Fatal(fasthttp.ListenAndServe(host, CORS(a.Router.Handler)))

	defer a.DB.Close()
}

func (a *App) handleRequest(handler RequestHandlerFunction) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		handler(ctx, a.DB)
		return
	}
}
