package controllers

import (
	"database/sql"
	"fmt"

	// "log"

	// "github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

// Index is the index handler
func Index(ctx *fasthttp.RequestCtx, db *sql.DB) {
	fmt.Fprint(ctx, "Welcome!\n")
}

// HealthStatus function
func HealthStatus(ctx *fasthttp.RequestCtx, db *sql.DB) {
	dbStatus := "OK"
	err := db.Ping()
	if err != nil {
		dbStatus = fmt.Sprintf("DB access error: %s", err)

	}
	healthStatusResponse := struct {
		DbStatus string `json:"dbStatus"`
	}{dbStatus}
	respondJSON(ctx, fasthttp.StatusOK, healthStatusResponse)
}
