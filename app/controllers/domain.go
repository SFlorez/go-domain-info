package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/valyala/fasthttp"

	"../models"
)

// DomainsResponse struct
type DomainsResponse struct {
	Items []models.Domain `json:"items"`
}

// GetAllDomains function
func GetAllDomains(ctx *fasthttp.RequestCtx, db *sql.DB) {
	domains, err := models.GetDomains(db)
	if err != nil {
		respondError(ctx, fasthttp.StatusInternalServerError, err.Error())
		return
	}

	domainResponse := DomainsResponse{
		Items: domains}

	respondJSON(ctx, fasthttp.StatusOK, domainResponse)
	return
}

// GetADomain function
func GetADomain(ctx *fasthttp.RequestCtx, db *sql.DB) {
	host := ctx.QueryArgs().Peek("host")
	errors := []string{}
	ssllabsResult, err := ssllabsAnalyze(host)

	if err != nil {
		respondError(ctx, fasthttp.StatusInternalServerError, err.Error())
		return
	}

	domain := &models.Domain{}
	domain.Host = ssllabsResult.Host

	if ssllabsResult.Status == "ERROR" {
		domain.IsDown = true
	}

	if err1 := domain.WebInfo(); err1 != nil {
		errors = append(errors, "Error scrapping web info: "+err1.Error())
	}

	if err2 := domain.UpdAteOrCreateDomain(db); err2 != nil {
		respondError(ctx, fasthttp.StatusInternalServerError, err2.Error())
		return
	}

	var lessSslGrade string = ""
	var lenServers int = len(ssllabsResult.Servers)
	servers := make([]models.Server, lenServers)

	for i := 0; i < lenServers; i++ {
		endpoint := &ssllabsResult.Servers[i]
		servers[i].Address = endpoint.IPAddress
		servers[i].Domain = ssllabsResult.Host
		servers[i].SslGrade = endpoint.SSLGrade
		servers[i].WhoisInfo()
		if err := servers[i].UpdAteOrCreateServer(db, endpoint.StatusMessage); err != nil {
			errors = append(errors, servers[i].Address+": "+err.Error())
		}
		if (lessSslGrade == "" || strings.Compare(lessSslGrade, servers[i].SslGrade) == 1) && servers[i].SslGrade != "" {
			lessSslGrade = servers[i].SslGrade
		}
	}

	server := &models.Server{}
	server.Domain = ssllabsResult.Host

	if err := server.GetServerUpdatedBefore(db); err != nil {
		errors = append(errors, "Error get server updated an hour ago or before: "+err.Error())
		return
	}

	domain.SSLGrade = lessSslGrade
	domain.ServersChanged = server.UpdatedAt.Valid
	domain.Servers = servers
	domain.Errors = errors

	if err := domain.UpdAteOrCreateDomain(db); err != nil {
		respondError(ctx, fasthttp.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(ctx, fasthttp.StatusOK, domain)
	return
}

func ssllabsAnalyze(host []byte) (models.SSLlabDomain, error) {
	var errorResponse error

	ssllabsResponse := fmt.Sprintf("https://api.ssllabs.com/api/v3/analyze?host=%s", host)
	_, body, err := fasthttp.Get(nil, ssllabsResponse)

	if err != nil {
		errorResponse = err
	}

	var sslLab models.SSLlabDomain
	json.Unmarshal(body, &sslLab)
	return sslLab, errorResponse
}
