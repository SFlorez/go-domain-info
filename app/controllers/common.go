package controllers

import (
	"encoding/json"

	"github.com/valyala/fasthttp"
)

// respondJSON makes the response with payload as json format
func respondJSON(ctx *fasthttp.RequestCtx, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		ctx.SetBody([]byte(err.Error()))
		return
	}

	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.SetStatusCode(status)
	ctx.SetBody([]byte(response))
}

// respondError makes the error response with payload as json format
func respondError(ctx *fasthttp.RequestCtx, code int, message string) {
	respondJSON(ctx, code, map[string]string{"error": message})
}
