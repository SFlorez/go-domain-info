package models

import (
	"database/sql"
	"fmt"

	"../services"
)

// Domain type is the fields that cover each item in database
type Domain struct {
	Host             string   `json:"host,pk,unique,notnull"`
	Title            string   `json:"title"`
	Logo             string   `json:"logo"`
	ServersChanged   bool     `json:"servers_changed"`
	SSLGrade         string   `json:"ssl_grade"`
	PreviousSSLGrade string   `json:"previous_ssl_grade,null"`
	IsDown           bool     `json:"is_down"`
	Servers          []Server `json:"servers"`
	Errors           []string `json:"errors"`
}

const (
	getDomainQry    = "SELECT host, title FROM domains WHERE id=$1"
	getDomainsQuery = "SELECT host, title, logo, servers_changed, ssl_grade, coalesce(previous_ssl_grade, ''), is_down FROM domains WHERE host != '' AND host IS NOT NULL"
	updateDomainQry = "UPDATE domains SET host=$1, title=$2 WHERE id=$3"
	deleteDomainQry = "DELETE FROM domains WHERE id=$1"
	createDomainQry = "INSERT INTO domains(host, title, logo, servers_changed, ssl_grade, previous_ssl_grade, is_down) VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING host"
	upsertDomainQry = "INSERT INTO domains(host, title, logo, servers_changed, ssl_grade, is_down) " +
		"VALUES($1, $2, $3, $4, $5, $6) " +
		"ON CONFLICT (host) " +
		"DO UPDATE SET title = EXCLUDED.title, logo = EXCLUDED.logo, servers_changed = EXCLUDED.servers_changed, ssl_grade = EXCLUDED.ssl_grade, " +
		"previous_ssl_grade = CASE WHEN (domains.ssl_grade IS NOT NULL) AND (domains.ssl_grade <> '') THEN domains.ssl_grade " +
		"ELSE CASE WHEN (domains.previous_ssl_grade IS NOT NULL) AND (domains.previous_ssl_grade <> '') THEN domains.previous_ssl_grade ELSE '' END END, " +
		"is_down = EXCLUDED.is_down " +
		"RETURNING host, title, logo, servers_changed, ssl_grade, coalesce(previous_ssl_grade, ''), is_down"
)

// CreateDomain function
func (d *Domain) CreateDomain(db *sql.DB) error {
	err := db.QueryRow(createDomainQry, d.Host, d.Title, d.Logo, d.ServersChanged, d.SSLGrade, d.PreviousSSLGrade, d.IsDown).Scan(&d.Host)
	if err != nil {
		return err
	}

	return nil
}

// UpdAteOrCreateDomain function
func (d *Domain) UpdAteOrCreateDomain(db *sql.DB) error {
	err := db.QueryRow(upsertDomainQry, d.Host, d.Title, d.Logo, d.ServersChanged, d.SSLGrade, d.IsDown).Scan(&d.Host, &d.Title, &d.Logo, &d.ServersChanged, &d.SSLGrade, &d.PreviousSSLGrade, &d.IsDown)
	if err != nil {
		return err
	}

	return nil
}

// GetDomains separate function to get multiple domains
func GetDomains(db *sql.DB) ([]Domain, error) {
	rows, err := db.Query(getDomainsQuery)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	domains := []Domain{}

	for rows.Next() {
		var d Domain
		if err := rows.Scan(&d.Host, &d.Title, &d.Logo, &d.ServersChanged, &d.SSLGrade, &d.PreviousSSLGrade, &d.IsDown); err != nil {
			return nil, err
		}
		domains = append(domains, d)
	}

	return domains, nil
}

// WebInfo function
func (d *Domain) WebInfo() error {
	web := &services.Scrapper{}
	url := fmt.Sprintf("https://www.%s", d.Host)

	if err := web.WebInfo(url); err != nil {
		return err
	}

	d.Logo = web.Icon
	d.Title = web.Title

	return nil
}
