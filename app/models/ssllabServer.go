package models

// SSLlabServer type
type SSLlabServer struct {
	IPAddress     string `json:"ipAddress"`
	ServerName    string `json:"serverName"`
	SSLGrade      string `json:"grade"`
	OrgName       string `json:"OrgName"`
	Country       string `json:"Country"`
	StatusMessage string `json:"statusMessage"`
}
