package models

// SSLlabDomain type
type SSLlabDomain struct {
	Host    string         `json:"host"`
	Status  string         `json:"status"`
	Servers []SSLlabServer `json:"endpoints"`
}
