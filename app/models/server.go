package models

import (
	"database/sql"

	"../services"
)

// Server type is the fields that cover each item in database
type Server struct {
	Address   string       `json:"address,pk,unique,notnull"`
	Domain    string       `json:"domain,notnull"`
	SslGrade  string       `json:"ssl_grade"`
	Country   string       `json:"country"`
	Owner     string       `json:"owner"`
	UpdatedAt sql.NullTime `json:"updated_at"`
}

const (
	getServerQry    = "SELECT address, domain, ssl_grade, country, owner, updated_at FROM servers WHERE address=$1"
	getServersQuery = "SELECT host, title, logo, servers_changed, ssl_grade, coalesce(previous_ssl_grade, ''), is_down FROM servers LIMIT $1 OFFSET $2"
	updateServerQry = "UPDATE servers SET host=$1, title=$2 WHERE id=$3"
	deleteServerQry = "DELETE FROM servers WHERE id=$1"
	createServerQry = "INSERT INTO servers(host, title, logo, servers_changed, ssl_grade, previous_ssl_grade, is_down) VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING host"
	upsertServerQry = "INSERT INTO servers(address, domain, ssl_grade, country, owner) " +
		"VALUES($1, $2, $3, $4, $5) " +
		"ON CONFLICT (address) " +
		"DO UPDATE SET domain = EXCLUDED.domain, country = EXCLUDED.country, owner = EXCLUDED.owner, " +
		"ssl_grade = CASE WHEN (EXCLUDED.ssl_grade IS NOT NULL AND EXCLUDED.ssl_grade != '') THEN EXCLUDED.ssl_grade ELSE servers.ssl_grade END, " +
		"updated_at = CASE WHEN (EXCLUDED.domain != servers.domain OR (EXCLUDED.ssl_grade != servers.ssl_grade AND servers.ssl_grade != '') " +
		"OR EXCLUDED.country != servers.country OR EXCLUDED.owner != servers.owner) THEN NOW() " +
		"ELSE servers.updated_at END " +
		"RETURNING address, domain, ssl_grade, country, owner, updated_at"
	upsertServerQryNoSSLChange = "INSERT INTO servers(address, domain, ssl_grade, country, owner) " +
		"VALUES($1, $2, $3, $4, $5) " +
		"ON CONFLICT (address) " +
		"DO UPDATE SET domain = EXCLUDED.domain, country = EXCLUDED.country, owner = EXCLUDED.owner, " +
		"ssl_grade = CASE WHEN (EXCLUDED.ssl_grade IS NOT NULL AND EXCLUDED.ssl_grade != '') THEN EXCLUDED.ssl_grade ELSE servers.ssl_grade END, " +
		"updated_at = CASE WHEN (EXCLUDED.domain != servers.domain OR EXCLUDED.country != servers.country OR EXCLUDED.owner != servers.owner) THEN NOW() " +
		"ELSE servers.updated_at END " +
		"RETURNING address, domain, ssl_grade, country, owner, updated_at"
	getServerUpdatedBefOneHour = "SELECT address, domain, ssl_grade, country, owner, updated_at FROM servers WHERE domain=$1 AND " +
		"updated_at IS NOT NULL AND updated_at < now() - interval '1 hour' LIMIT 1"
)

// GetServer function
func (s *Server) GetServer(db *sql.DB) error {
	err := db.QueryRow(getServerQry, s.Address).Scan(&s.Address, &s.Domain, &s.SslGrade, &s.Country, &s.Owner, &s.UpdatedAt)
	if err != nil {
		return err
	}
	return nil
}

// UpdAteOrCreateServer function
func (s *Server) UpdAteOrCreateServer(db *sql.DB, statusMessage string) error {
	var query = upsertServerQryNoSSLChange

	if statusMessage == "Ready" {
		query = upsertServerQry
	}

	err := db.QueryRow(query, s.Address, s.Domain, s.SslGrade, s.Country, s.Owner).Scan(&s.Address, &s.Domain, &s.SslGrade, &s.Country, &s.Owner, &s.UpdatedAt)
	if err != nil {
		return err
	}
	return nil
}

// GetServerUpdatedBefore get server by domain updated an hour ago or before, limited to 1 response
func (s *Server) GetServerUpdatedBefore(db *sql.DB) error {
	err := db.QueryRow(getServerUpdatedBefOneHour, s.Domain).Scan(&s.Address, &s.Domain, &s.SslGrade, &s.Country, &s.Owner, &s.UpdatedAt)

	if err == nil {
		return err
	}
	return nil
}

// WhoisInfo function
func (s *Server) WhoisInfo() error {
	whois := &services.Whois{}

	if err := whois.WhoisInfo(s.Address); err != nil {
		return err
	}

	s.Owner = whois.OrgName
	s.Country = whois.Country

	return nil
}
