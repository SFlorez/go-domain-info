package services

import (
	"os/exec"
	"reflect"
	"strings"

	"github.com/likexian/gokit/assert"
)

// Whois struct
type Whois struct {
	OrgName string
	Country string
}

// WhoisInfo function
func (whois *Whois) WhoisInfo(address string) error {
	cmd := exec.Command("whois", address)
	stdout, err := cmd.CombinedOutput()

	if err != nil {
		return err
	}

	whoisStdout := string(stdout)
	whoisLines := strings.Split(whoisStdout, "\n")

	for i := 0; i < len(whoisLines); i++ {
		line := strings.TrimSpace(whoisLines[i])

		if len(line) > 5 && strings.Contains(line, ":") && (strings.Contains(line, "OrgName") || strings.Contains(line, "Country")) {
			fChar := line[:1]
			if assert.IsContains([]string{"-", "*", "%", ">", ";", "#"}, fChar) {
				continue
			}

			if line[len(line)-1:] == ":" {
				i++
				for ; i < len(whoisLines); i++ {
					thisLine := strings.TrimSpace(whoisLines[i])
					if strings.Contains(thisLine, ":") {
						break
					}
					line += thisLine + ","
				}
				line = strings.Trim(line, ",")
				i--
			}

			lines := strings.SplitN(line, ":", 2)
			name := strings.TrimSpace(lines[0])
			value := strings.TrimSpace(lines[1])
			value = strings.TrimSpace(strings.Trim(value, ":"))

			reflect.ValueOf(whois).Elem().FieldByName(name).SetString(value)

		}
	}

	return nil
}
