package services

import (
	"errors"
	"fmt"

	"github.com/PuerkitoBio/goquery"
)

// Scrapper struct
type Scrapper struct {
	Icon  string
	Title string
}

// WebInfo function
func (scrpp *Scrapper) WebInfo(host string) error {
	doc, err1 := goquery.NewDocument(host)

	if err1 != nil {
		return err1
	}

	title, err2 := getTitle(doc)
	if err2 != nil {
		return err2
	}
	scrpp.Title = title

	icon, err3 := getLinkImage(doc)
	if err3 != nil {
		return err3
	}
	scrpp.Icon = icon

	return nil
}

// GetLinkImage from web
func getLinkImage(doc *goquery.Document) (string, error) {
	attrConditionals := []string{"icon", "shortcut icon"}
	sel, err := getTagByAttrCond(doc, "link", "rel", attrConditionals)
	var value string = ""

	if err != nil {
		return value, err
	}

	iconSrc, exist := sel.Attr("href")

	if exist {
		return iconSrc, nil
	}

	return value, errors.New("Logo href does not exist")
}

func getTitle(doc *goquery.Document) (string, error) {
	var value string = ""
	var exist bool = false

	doc.Find("head title").Each(func(i int, s *goquery.Selection) {
		value = s.Text()
		exist = true
		return
	})
	if exist {
		return value, nil
	}
	return value, errors.New("Title does not exist")
}

func getTagByAttrCond(doc *goquery.Document, tag string, attr string, attrCond []string) (*goquery.Selection, error) {
	var sel *goquery.Selection
	var exist bool = false
	query := fmt.Sprintf("head %s", tag)

	doc.Find(query).Each(func(i int, s *goquery.Selection) {
		itemAttr, exts := s.Attr(attr)

		if exts && contains(attrCond, itemAttr) {
			sel = s
			exist = true
			return
		}
	})

	if exist {
		return sel, nil
	}

	return sel, errors.New("Icon does not exist")
}

func contains(array []string, item string) bool {
	for i := 0; i < len(array); i++ {
		if array[i] == item {
			return true
		}
	}

	return false
}
