CREATE DATABASE IF NOT EXISTS domain_info;

CREATE TABLE IF NOT EXISTS domain_info.domains (
  host STRING NOT NULL PRIMARY KEY,
  title STRING NULL,
  logo STRING NULL,
  servers_changed BOOL NULL,
  ssl_grade STRING NULL,
  previous_ssl_grade STRING NULL,
  is_down BOOL NULL
);

CREATE TABLE IF NOT EXISTS domain_info.servers (
  address STRING PRIMARY KEY,
  domain STRING NOT NULL REFERENCES domain_info.domains (host) ON DELETE CASCADE,
  ssl_grade STRING NULL,
  country STRING NULL,
  owner STRING NULL,
  updated_at TIMESTAMP NULL,
  INDEX (domain)
);