package main

import (
	"fmt"
	"os"

	"./app"
)

var err error

func main() {
	a := &app.App{}
	appPort := fmt.Sprintf(":%s", os.Getenv("APP_PORT"))

	a.Initialize()
	a.Run(appPort)
}
